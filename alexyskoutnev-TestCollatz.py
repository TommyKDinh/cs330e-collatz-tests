#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    
    def test_read_1(self):
        s = "13 20\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 13)
        self.assertEqual(j, 20)
    
    def test_read_2(self):
        s = "2\n"
        self.assertRaises(IndexError, collatz_read,s) #Failure case
        
    def test_read_3(self): #Corner case if reverse inputs work
        s = "9 5\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 9)
        self.assertEqual(j, 5)
    
    def test_read_3(self): #Corner Case
        s = "1 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 1)
        

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
    
    def test_eval_5(self): 
        self.assertRaises(AssertionError, collatz_eval, -1, 5) #Failure case
        
    def test_eval_6(self): #Corner case where lower and upper bounds are same
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)
       
    def test_eval_7(self): #Corner case if reverse inputs work
        v = collatz_eval(9, 5)
        self.assertEqual(v, 20)
    
    def test_eval_8(self):
        v = collatz_eval(22, 123)
        self.assertEqual(v, 119)
        
    def test_eval_9(self): #Large possible Corner Case
        v = collatz_eval(100999, 999999)
        self.assertEqual(v, 525)
        
    def test_eval_10(self):
        v = collatz_eval(4923, 232211)
        self.assertEqual(v, 443)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    
    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 5, 100000, 351)
        self.assertEqual(w.getvalue(), "5 100000 351\n")
    
    def test_print_2(self): #Corner case where lower and upper bounds are same
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")
        
    def test_print_3(self): #Corner case if reverse inputs work
        w = StringIO()
        collatz_print(w, 91, 2, 116)
        self.assertEqual(w.getvalue(), "91 2 116\n")
        
    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    
    def test_solve_1(self):
        r = StringIO("4 10\n55 6\n9999 100000\n23 29\n")    
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "4 10 20\n55 6 113\n9999 100000 351\n23 29 112\n")
        
    def test_solve_2(self):
        r = StringIO("1 1\n2 2\n10 99\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1 1 1\n2 2 2\n10 99 119\n")

    def test_solve_3(self):
        r = StringIO("299 23\n10 1000\n3 9\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "299 23 128\n10 1000 179\n3 9 20\n")
    
    def test_solve_4(self): #Maximum Test Case
        r = StringIO("1 999999\n422 923832\n9239 233321\n")
        w = StringIO()
        collatz_solve(r,w)
        self.assertEqual(w.getvalue(), "1 999999 525\n422 923832 525\n9239 233321 443\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
