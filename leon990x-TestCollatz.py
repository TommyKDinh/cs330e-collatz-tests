#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    def test_read_1(self):
        s = "199 180\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  199)
        self.assertEqual(j, 180)
        
    def test_read_2(self):
        s = "499 550\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  499)
        self.assertEqual(j, 550)
    
    def test_read_3(self):
        s = "999 1010\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 999)
        self.assertEqual(j, 1010)
    # ----

    def test_read_(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # ----
    # eval
    def test_eval_5(self):
        v = collatz_eval(700, 700)
        self.assertEqual(v, 83)
        
    def test_eval_6(self):
        v = collatz_eval(310, 301)
        self.assertEqual(v, 87)
    
    def test_eval_7(self):
        v = collatz_eval(401, 410)
        self.assertEqual(v, 41)
    
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # -----
    # print
    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 2000, 1901, 175)
        self.assertEqual(w.getvalue(), "2000 1901 175\n")
    
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 5280, 5300, 148)
        self.assertEqual(w.getvalue(), "5280 5300 148\n")
     
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 4200, 4300, 202)
        self.assertEqual(w.getvalue(), "4200 4300 202\n")
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    
    def test_solve_1(self):
        r = StringIO("30 20\n40 50\n60 70\n70 80\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "30 20 112\n40 50 110\n60 70 108\n70 80 116\n")
    
    def test_solve_2(self):
        r = StringIO("150 140\n170 160\n180 190\n195 199\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "150 140 117\n170 160 112\n180 190 107\n195 199 120\n")
            
    def test_solve_3(self):
        r = StringIO("850 950\n800 700\n600 500\n499 301\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "850 950 179\n800 700 171\n600 500 137\n499 301 144\n")
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
